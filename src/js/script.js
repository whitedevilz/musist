// like
$(".like .fa-heart").click(function (e) { 
    $(this).toggleClass("active");
});

// read more
$("#read_more_post_1").click(function (e) { 
    $(".read-more-content_post_1").show();
});
$("#go_back_post_1").click(function (e) { 
    $(".read-more-content_post_1").hide();
});

// comment
$(".comment").click(function (e) { 
    $("#comment-box").show();
});
$("#comment-box-hide").click(function (e) { 
    $("#comment-box").hide();
});